<?php
/**
 * @file
 * rlab_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function rlab_views_views_api() {
  return array("api" => "3.0");
}
